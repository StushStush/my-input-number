import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Input
} from './Components/input.tsx';

function App() {
  return (
    <div className="App" >
      <Input />
    </div>
  );
}

export default App;