import * as React from 'react';
import InputMask from 'react-input-mask';
import './input.css';

//условные данные с бэка
const initialValues: Array<string> = ["+7-999-999-99-99", "+7-999-999-00-00"]

interface IPhoneInput {
  value: string;
  color: string;
  error: boolean;
}

export class Input extends React.Component {
  state: IPhoneInput = {
    value: '',
    color: 'grey',
    error: false,
  }

  onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      value: event.target.value
    });
  }

  onBlur = () => {
    if (initialValues.includes(this.state.value) && this.state.value.length === 16) {
      this.setState({
        color: 'green',
        error: false
      });
    } else if (!initialValues.includes(this.state.value) && this.state.value.length === 16) {
      this.setState({
        color: 'yellow',
        error: true
      });
    } else if (this.state.value.length < 16 && this.state.value.length > 4) {
      //не делала ручную проверку на маску, так как библиотека не дает возможность ввода не по маске
      this.setState({
        color: 'red',
        error: true,
      });
    } else if (this.state.value.length < 4) {
      this.setState({
        color: 'grey',
        error: false
      });
    }

  }

  render(): JSX.Element {
    return <div>
      {this.state.error &&
        <div className={'error-message'}>
          <p>Такого номера нет или вы ввели недостаточно символов</p>
        </div>
      }
      <div className={'form-input-wrap'}>
        <p className={'form-title'}>
          Введите номер телефона
     </p>
        <InputMask
          className={`form-input form_input_${this.state.color}`}
          mask="+7-999-999-99-99"
          maskChar={null}
          placeholder="+7-999-999-99-99"
          value={this.state.value}
          onBlur={this.onBlur}
          onChange={this.onChange}
        />
      </div>
    </div>



  }
}